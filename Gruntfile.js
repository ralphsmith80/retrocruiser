module.exports = function (grunt) {

    require('time-grunt')(grunt);

    var loadConfig = require('load-grunt-config');

    loadConfig(grunt, {
        configPath: __dirname + '/tasks/options',
        config: {
            release_dir: 'build',
            compile_dir: 'dist',
            source_dir: 'src',
            vendor_dir: 'vendor',
            // banner: require('fs').readFileSync(__dirname + '/tasks/banner.txt', 'utf8')
        },
        // init: false,
        // jitGrunt: true
        // data: {}
        jitGrunt: {
            customTasksDir: 'tasks',
            // staticMappings: {
            //     sprite: 'grunt-spritesmith'
            // }
        }
    });

    // grunt.loadTasks('tasks');

    grunt.registerTask('default', ['build']);

    // grunt.registerTask('build', ['clean', 'jshint', 'concat', 'uglify']);
    grunt.registerTask('build', ['clean', 'concat', 'uglify']);

    // grunt.registerTask('dist', ['replace', 'build', 'copy']);
    grunt.registerTask('dist', ['build', 'copy']);

};
