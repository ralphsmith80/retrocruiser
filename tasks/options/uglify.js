
module.exports = {
    standalone: {
        options: {
            // sourceMap: true,
            // sourceMapName: '<%= compile_dir %>/phaser.map',
            // banner: '/* Phaser v<%= package.version %> - http://phaser.io - @photonstorm - (c) 2014 Photon Storm Ltd. */\n'
        },
        src: ['<%= concat.standalone.dest %>'],
        dest: '<%= compile_dir %>/index.min.js'
    }
};
