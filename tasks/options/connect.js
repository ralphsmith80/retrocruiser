module.exports = {
    root: {
        options: {
            keepalive: true,
            hostname: '*',
            open: true
        }
    },
    production: {
        options: {
            keepalive: true,
            base: 'build',
            hostname: '*',
            open: true
        }
    }
};
