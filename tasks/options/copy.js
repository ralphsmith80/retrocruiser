module.exports = {
    main: {
        files: [
            { src: ['vendor/production.phaser.min.js'], dest: 'build/production.phaser.min.js' },
            { src: ['vendor/phaser.map'], dest: 'build/phaser.map' },
            { src: ['dist/index.min.js'], dest: 'build/index.min.js' },
            { src: ['assets/**'], dest: 'build/', expand: true },
            { src: ['css/**'], dest: 'build/', expand: true },
        ]
    }
};
