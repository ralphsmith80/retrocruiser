module.exports = {

    // //  Phaser with Pixi and Arcade Physics, but no Ninja or P2 libs
    // phaserArcadePhysics: {
    //     options: {
    //         banner: '<%= banner %>'
    //     },
    //     src: ['<%= compile_dir %>/pixi.js', '<%= compile_dir %>/phaser-no-libs.js'],
    //     dest: '<%= compile_dir %>/phaser-arcade-physics.js'
    // },

    //  One ring to rule them all
    standalone: {
        options: {
            // banner: '<%= banner %>'
        },
        src: [
            '<%= vendor_dir %>/phaser.min.js',

            '<%= source_dir %>/states/boot.js',
            '<%= source_dir %>/utils.js',
            '<%= source_dir %>/states/preloader.js',
            '<%= source_dir %>/states/mainmenu.js',
            '<%= source_dir %>/states/game.js',
            '<%= source_dir %>/states/gameover.js',
             // utility
            '<%= source_dir %>/recyclegroup.js',
             // generators
            '<%= source_dir %>/boosts/boostgenerator.js',
            '<%= source_dir %>/weapons/weapongenerator.js',
            '<%= source_dir %>/baddies/rockgenerator.js',
            '<%= source_dir %>/environment/envgenerator.js',
             // fx
            '<%= source_dir %>/fx/explosion.js',
             // game stage
            '<%= source_dir %>/scoreboard.js',
            '<%= source_dir %>/heroselect.js',
            '<%= source_dir %>/ground.js',
             // environment
            '<%= source_dir %>/environment/environment.js',
            '<%= source_dir %>/environment/bush.js',
             // weapons
            '<%= source_dir %>/weapons/weapon.js',
            '<%= source_dir %>/weapons/weakweapon.js',
            '<%= source_dir %>/weapons/mediumweapon.js',
            '<%= source_dir %>/weapons/strongweapon.js',
             // heros
            '<%= source_dir %>/heros/hero.js',
            '<%= source_dir %>/heros/hornet.js',
            '<%= source_dir %>/heros/wwing.js',
            '<%= source_dir %>/heros/bwing.js',
             // baddies
            '<%= source_dir %>/baddies/rock.js',
            '<%= source_dir %>/baddies/rock_small0.js',
            '<%= source_dir %>/baddies/rock_small1.js',
            '<%= source_dir %>/baddies/rock_medium0.js',
            '<%= source_dir %>/baddies/rock_medium1.js',
            '<%= source_dir %>/baddies/rock_big.js',
             // boosts
            '<%= source_dir %>/boosts/boost.js',
            '<%= source_dir %>/boosts/fuelboost.js',
            '<%= source_dir %>/boosts/healthboost.js',
            '<%= source_dir %>/boosts/mediumweaponboost.js',
            '<%= source_dir %>/boosts/strongweaponboost.js',
             // controller
            '<%= source_dir %>/controller.js',
            // main index.js
            '<%= source_dir %>/index.js'
        ],
        dest: '<%= compile_dir %>/index.js'
    }

};
