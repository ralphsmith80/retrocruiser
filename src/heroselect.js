BasicGame.HeroGroup = function(game) {
    // var self = this,
    //     heros = {
    //         bwing:  'b-wing',
    //         hornet: 'hornet',
    //         wwing:  'w-wing'
    //     };
    // Phaser.Group.call(this, game);
    // Object.keys(heros).forEach(function(hero) {
    //     var sprite = self.create(100, 100, heros[hero]);
    //     sprite.name = heros[hero];
    //     if (sprite.name === 'w-wing' || sprite.name === 'b-wing') {
    //         sprite.setFrame(2);
    //         sprite.scale.setTo(2, 2);
    //     }
    //     if (sprite.name === 'hornet') {
    //         sprite.setFrame(4);
    //         spacing = sprite.width;
    //     }
    // });
    // // position in a line
    // var i = 0;
    // this.forEach(function(sprite) {
    //     sprite.position.setTo(i*spacing, 0);
    //     i++;
    // });
    var bwing, hornet, wwing;
    Phaser.Group.call(this, game);

    // far right
    hornet = this.create(0, 0, 'hornet', 4);
    hornet.name = 'hornet';
    hornet.class = BasicGame.Hornet;
    this.spacing = hornet.width;
    // middle
    wwing = this.create(0+hornet.width, 0, 'w-wing', 2);
    wwing.name = 'w-wing';
    wwing.class = BasicGame.WWing;
    wwing.scale.setTo(2, 2);
    // far left
    bwing = this.create(0+hornet.width*2, 0, 'b-wing', 2);
    bwing.name = 'b-wing';
    bwing.class = BasicGame.BWing;
    bwing.scale.setTo(2, 2);

    this.setAll('anchor.x', 0.5);
    this.setAll('anchor.y', 0.5);

    // FF and Safari get mad if you don't use an INT
    this.resetTint = parseInt('0xffffff');
    this.selectedTint = parseInt('0xffffff');

    // start with the center sprite selected
    // i.e. set the group cursor to the center sprite
    this.resetCursor(1);
    this.cursor.tint = this.selectedTint;
    this.cursor.alpha = 1;
    // this.fade();
    this.tweening = false;

    // enable input for sprites in group and
    // set cursor to sprite when clicked
    this.forEach(function(sprite) {
        sprite.inputEnabled = true;
        sprite.events.onInputDown.add(function(sprite) {
            var cursor = sprite;
            this.cursor = cursor;
            this.selectedCallbacks.forEach(function(listner) {
                listner.callback.call(listner.context, cursor);
            });
        }, this);
    }, this);
    // so other objects can register for updates when hero is selected
    this.selectedCallbacks = [];
};
BasicGame.HeroGroup.prototype = Object.create(Phaser.Group.prototype);
BasicGame.HeroGroup.constructor = BasicGame.HeroGroup;

BasicGame.HeroGroup.prototype.fade = function() {
    this.cursor.alpha = 1;
    this.forEach(function(sprite) {
        if (this.cursor === sprite) return;
        sprite.alpha = 0.5;
    }, this);
};
BasicGame.HeroGroup.prototype.offSelected = function(callback, context) {
    this.selectedCallbacks = [];
    return this;
};
BasicGame.HeroGroup.prototype.onSelected = function(callback, context) {
    this.selectedCallbacks.push({
        callback: callback,
        context: context
    });
    return this;
};
BasicGame.HeroGroup.prototype.rerender = function() {
    var i=0;
    this.children.forEach(function(sprite) {
    // this.forEach(function(sprite) {
        sprite.position.setTo(i*this.spacing, 0);
        i++;
    }, this);
};
BasicGame.HeroGroup.prototype.rotateLeft = function() {
    var child;
    this.cursor.tint = this.resetTint;
    this.children.push(this.children.shift());
    this.rerender();
    // make sure we keep the cursor on the center sprite
    child = this.resetCursor(1);
    this.cursor.tint = this.selectedTint;
    this.fade();
    return child;
};
BasicGame.HeroGroup.prototype.rotateRight = function() {
    var child;
    this.cursor.tint = this.resetTint;
    this.children.unshift(this.children.pop());
    this.rerender();
    // make sure we keep the cursor on the center sprite
    child = this.resetCursor(1);
    this.cursor.tint = this.selectedTint;
    this.fade();
    return child;
};
BasicGame.HeroGroup.prototype.shiftLeft = function() {
    if (this.tweening) return this;
    if (this.cursor === this.getBottom()) return this;
    this.previous();
    this.shiftTween(this.position.x + this.width/3);
    return this;
};
BasicGame.HeroGroup.prototype.shiftRight = function() {
    if (this.tweening) return this;
    if (this.cursor === this.getTop()) return this;
    this.next();
    this.shiftTween(this.position.x - this.width/3);
    return this;
};
BasicGame.HeroGroup.prototype.shiftTween = function(x) {
    this.tweening = true;
    var tween = this.game.add.tween(this).to({x: x}, 500, Phaser.Easing.Linear.None, true);
    // dim while moving
    this.forEach(function(sprite) {
        sprite.alpha = 0.5;
    }, this);
    // make middle opac
    tween.onComplete.addOnce(function() {
        this.tweening = false;
        this.fade();
    }, this);
    return this;
};
