
BasicGame.MediumWeapon = function(game, x, y, frame) {
    var name = 'medium-weapon';
    BasicGame.Weapon.call(this, game, x, y, name, frame);
};
BasicGame.MediumWeapon.prototype = Object.create(BasicGame.Weapon.prototype);
BasicGame.MediumWeapon.constructor = BasicGame.MediumWeapon;
BasicGame.MediumWeapon.prototype._init = function() {
    this.damage = 10;
    this.lifespan = 1500;
    this.fireRate = 100;
    this.fireTime = 0;
    this.offset = 48;
    this.body.velocity.setTo(0, -400);
    return this;
};
