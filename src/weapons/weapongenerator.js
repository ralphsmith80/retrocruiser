BasicGame.WeaponGenerator = function(game, Weapon) {
    BasicGame.RecycleGroup.call(this, game, Weapon);

    // This is a bit wierd but the fireRate and fireTime should be
    // defined by the Weapon so we need an instance of that.
    // We don't just out-right define them here because each weapon is different
    // and we don't want to have to sub-class the WeaponGenerator everytime
    // That's ok we add the first on to the group so it will be used
    var weapon = new Weapon(game)._init();
    this.add(weapon);
    this.fireRate = weapon.fireRate;
    this.fireTime = weapon.fireTime;
};
BasicGame.WeaponGenerator.prototype = Object.create(BasicGame.RecycleGroup.prototype);
BasicGame.WeaponGenerator.constructor = BasicGame.WeaponGenerator;
// delegate the attack method to the weapon sprite
BasicGame.WeaponGenerator.prototype.attack = function(x, y) {
    var sprite = this.spawn(x, y);
    if (sprite) {
        sprite.attack();
    }
};
// define the spawn behavior for a weapon
BasicGame.WeaponGenerator.prototype.spawn = function(x, y) {
    var sprite = null,
        now = this.game.time.now;
    if (now > this.fireTime + this.fireRate) {
        this.fireTime = now + this.fireRate;
        sprite = BasicGame.RecycleGroup.prototype.spawn.call(this, x, y);
    }
    return sprite;
};
