// weapon base class/interface
BasicGame.Weapon = function(game, x, y, name, frame) {
    Phaser.Sprite.call(this, game, x, y, name, frame);
    this.name = name;
    this.anchor.setTo(0.5, 0.5);

    this.game.physics.arcade.enableBody(this);
    this.checkWorldBounds = true;
    this.outOfBoundsKill = true;

    // this.sound = this.game.add.audio('weapon1', 0.5);

    // this.sound = this.game.add.audio('weapon2', 0.5);
    // this.sound = this.game.add.audio('weapon3', 1);

    // this.sound = this.game.add.audio('weapon4', 1);
    this.sound = this.game.add.audio('weapon5', 1);
    // this.sound = this.game.add.audio('weapon6', 1);
    // this.sound = this.game.add.audio('weapon7', 1);
};
BasicGame.Weapon.prototype = Object.create(Phaser.Sprite.prototype);
BasicGame.Weapon.constructor = BasicGame.Weapon;
BasicGame.Weapon.prototype._init = function() {
    return this;
};
BasicGame.Weapon.prototype.attack = function() {
    this.sound.play();
    return this;
};
BasicGame.Weapon.prototype.reset = function() {
    Phaser.Sprite.prototype.reset.apply(this, arguments);
    this._init();
    return this;
};
