
BasicGame.StrongWeapon = function(game, x, y, frame) {
    var name = 'strong-weapon';
    BasicGame.Weapon.call(this, game, x, y, name, frame);
};
BasicGame.StrongWeapon.prototype = Object.create(BasicGame.Weapon.prototype);
BasicGame.StrongWeapon.constructor = BasicGame.StrongWeapon;
BasicGame.StrongWeapon.prototype._init = function() {
    this.damage = 20;
    this.lifespan = 1500;
    this.fireRate = 125;
    this.fireTime = 0;
    this.offset = 32;
    this.body.velocity.setTo(0, -400);
    return this;
};
