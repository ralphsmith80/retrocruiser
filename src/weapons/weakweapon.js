BasicGame.WeakWeapon = function(game, x, y, frame) {
    var name = 'weak-weapon';
    BasicGame.Weapon.call(this, game, x, y, name, frame);
};
BasicGame.WeakWeapon.prototype = Object.create(BasicGame.Weapon.prototype);
BasicGame.WeakWeapon.constructor = BasicGame.WeakWeapon;
BasicGame.WeakWeapon.prototype._init = function() {
    this.damage = 5;
    this.lifespan = 1500;
    this.fireRate = 50;
    this.fireTime = 0;
    this.offset = 32;
    this.body.velocity.setTo(0, -400);
    return this;
};
