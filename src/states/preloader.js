
BasicGame.Preloader = function(game) {
    // 2 second fade
    this.fadeDuration = 2000;
};

BasicGame.Preloader.prototype = {

    preload: function() {
        // Preload sprites
        //  A nice sparkly background and a loading progress bar
        this.background = this.add.sprite(0, 0, 'title');
        this.background.width = this.game.width;
        this.background.height = this.game.height;
        this.preloadBar = this.add.sprite(this.game.world.centerX-200, this.game.world.height-100, 'loading');
        this.preloadBar.anchor.setTo(0, 0.5);

        //  This sets the preloadBar sprite as a loader sprite.
        //  What that does is automatically crop the sprite from 0 to full-width
        //  as the files below are loaded in.
        this.load.setPreloadSprite(this.preloadBar);

        // font
        // this.game.load.bitmapFont('lcd', 'assets/fonts/lcd_pixel/font.png', 'assets/fonts/lcd_pixel/font.fnt');

        // Audio
        // @params (key, urls, autoDecode)
        this.game.load.audio('title_sound', [
            'assets/audio/title/Pulsing_Sweep.ogg',
            'assets/audio/title/Pulsing_Sweep.mp3'
        ], true);
        // this.game.load.audio('game_over0', ['assets/audio/title/SuperStager.ogg'], true);
        this.game.load.audio('game_over1', [
            'assets/audio/title/SyntheticDesign.ogg',
            'assets/audio/title/SyntheticDesign.mp3'
        ], true);

        this.game.load.audio('stage0', [
            'assets/audio/stage/OverdriveSexMachine.ogg',
            'assets/audio/stage/OverdriveSexMachine.mp3'
        ], true);
        // this.game.load.audio('stage1', ['assets/audio/stage/bomberguy.wav'], true);
        // this.game.load.audio('stage2', ['assets/audio/stage/twin-turbo.wav'], true);

        // this.game.load.audio('explosion0', ['assets/audio/fx/explosion.m4a'], true);
        // this.game.load.audio('explosion1', ['assets/audio/fx/explode.wav'], true);
        this.game.load.audio('explosion2', [
            'assets/audio/fx/dark_explosion.ogg',
            'assets/audio/fx/dark_explosion.mp3'
        ], true);

        this.game.load.audio('boost', [
            'assets/audio/fx/boost.ogg',
            'assets/audio/fx/boost.mp3'
        ], true);

        // this.game.load.audio('weapon0', ['assets/audio/weapon/gravity_bomb.wav'], true);
        // this.game.load.audio('weapon1', ['assets/audio/weapon/missile_launch.wav'], true);
        // this.game.load.audio('weapon2', ['assets/audio/weapon/shot1.wav'], true);
        // this.game.load.audio('weapon3', ['assets/audio/weapon/shot2.wav'], true);
        // this.game.load.audio('weapon4', ['assets/audio/weapon/shotgun.wav'], true);
        this.game.load.audio('weapon5', [
            'assets/audio/weapon/SynthZapImpact.ogg',
            'assets/audio/weapon/SynthZapImpact.mp3'
        ], true);
        // this.game.load.audio('weapon6', ['assets/audio/weapon/WarpEngineering.ogg'], true);
        // this.game.load.audio('weapon7', ['assets/audio/weapon/zzzt.wav'], true);

        // this.game.load.audio('thrust0', ['assets/audio/ship/engine_loop.wav'], true);
        // this.game.load.audio('thrust1', ['assets/audio/ship/jetwashloop.wav'], true);
        this.game.load.audio('thrust2', [
            'assets/audio/ship/space-ship-engine-inside.ogg',
            'assets/audio/ship/space-ship-engine-inside.mp3'
        ], true);
        // this.game.load.audio('thrust3', ['assets/audio/ship/StarDriveEngaged.ogg'], true);
        // this.game.load.audio('thrust4', ['assets/audio/ship/StarDriveRumble.ogg'], true);
        this.game.load.audio('die', [
            'assets/audio/ship/player_death.ogg',
            'assets/audio/ship/player_death.mp3'
        ], true);

        this.game.load.audio('alarm', [
            'assets/audio/ship/Alarm.ogg',
            'assets/audio/ship/Alarm.mp3'
        ], true);
        this.game.load.audio('fall0', [
            'assets/audio/ship/falling0-short.ogg',
            'assets/audio/ship/falling0-short.mp3'
        ], true);
        // this.game.load.audio('fall1', ['assets/audio/ship/falling1-short.ogg'], true);

        // Stage
        this.game.load.image('energy', 'assets/images/ship/Energy.bmp');
        this.game.load.image('fuel', 'assets/images/ship/Fuel.bmp');
        // this.game.load.image('ground', 'assets/images/ground/bkg0-4x.png');
        this.game.load.image('ground', 'assets/images/ground/bkg0.bmp');
        // this.game.load.image('ground', 'assets/images/ground/grass1.png');
        // this.game.load.image('ground', 'assets/images/ground/starfield.jpg');
        // this.game.load.image('ground', 'assets/images/ground/map-newyork.png');

        // Environment
        this.game.load.image('bush', 'assets/images/environment/bush.png');

        // Ship
        this.game.load.spritesheet('w-wing', 'assets/images/ship/tyrian_ship_0.png', 119/5, 25, 5);
        this.game.load.spritesheet('b-wing', 'assets/images/ship/tyrian_ship_1.png', 119/5, 25, 5);
        this.game.load.spritesheet('hornet', 'assets/images/ship/plane-sheet.png', 640/10, 64, 10);
        this.game.load.image('hornet-shadow', 'assets/images/ship/plane-shadow.png');

        // Boosts
        // this.game.load.image('progress', 'assets/images/ship/Pearl.bmp');
        this.game.load.image('fuel-boost', 'assets/images/boosts/Juice.bmp');
        this.game.load.image('health-boost', 'assets/images/boosts/firstaid.png');

        // FX
        this.game.load.spritesheet('explode', 'assets/images/fx/explode1.png', 2048/16, 128, 16);
        // this.game.load.spritesheet('explode', 'assets/images/fx/explosion.png', 320/5, 320/5, 23);
        this.game.load.image('corona', 'assets/images/particles/blue.png');
        // this.game.load.image('bubble', 'assets/images/particles/bubble.png');
        this.game.load.image('white', 'assets/images/particles/white.png');

        // Baddies
        this.game.load.image('rock0', 'assets/images/baddies/Rock0.bmp');
        this.game.load.image('rock1', 'assets/images/baddies/Rock1.bmp');
        this.game.load.image('rock2', 'assets/images/baddies/Rock2.bmp');
        this.game.load.image('rock3', 'assets/images/baddies/Rock3.bmp');
        this.game.load.image('rock4', 'assets/images/baddies/Rock4.bmp');

        // Weapons
        this.game.load.image('weak-weapon', 'assets/images/weapons/weakweapon.png');
        this.game.load.image('medium-weapon', 'assets/images/weapons/mediumweapon.png');
        this.game.load.image('strong-weapon', 'assets/images/weapons/strongweapon.png');

        // controller
        // this.game.load.spritesheet('buttonhorizontal', 'assets/images/buttons/big-button-horizontal.png', 96, 64);
        // this.game.load.spritesheet('buttonvertical', 'assets/images/buttons/big-button-vertical.png', 64, 64);
        this.game.load.spritesheet('buttondiagonal', 'assets/images/buttons/big-button-diagonal.png', 64, 64);
        this.game.load.spritesheet('buttonfire', 'assets/images/buttons/big-button-round.png', 96, 96);
        this.game.load.spritesheet('horizontal', 'assets/images/buttons/horizontal.png', 96, 48);
        this.game.load.spritesheet('vertical', 'assets/images/buttons/vertical.png', 48, 96);
        this.game.load.spritesheet('strife', 'assets/images/buttons/strife.png', 96, 96);
        this.game.load.spritesheet('left-arrow', 'assets/images/buttons/left-arrow.png', 48, 48);
        this.game.load.spritesheet('right-arrow', 'assets/images/buttons/right-arrow.png', 48, 48);
    },
    create: function() {
        this.preloadBar.visible = false;

        // @params (key, volume, loop, connect)
        BasicGame.music = this.game.add.audio('title_sound', 1, true);
        BasicGame.music.play();
        // fade out music
        // this.game.add.tween(BasicGame.music).to({volume: 0}, 4000, Phaser.Easing.Linear.None, true);

        this.add.tween(this.background)
            // this first tween doesn't do anything other than provide wait time
            // before the fadeout tween starts
            .from({alpha: 1}, this.fadeDuration, Phaser.Easing.Linear.None)
            .to({alpha: 0}, this.fadeDuration, Phaser.Easing.Linear.None)
            .start()
            // must call complete on `_lastChild` or it will be called at the end of first tween
            ._lastChild.onComplete.add(function() {
                this.game.state.start('MainMenu');
            }, this);
    },
    update: function() {
    }
};
