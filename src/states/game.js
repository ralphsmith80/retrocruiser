BasicGame.Game = function(game) {
    this.player = null;
    this.ground = null;
    BasicGame.score = 0;
    this.scoreText = null;
    this.game = game;
};

BasicGame.Game.prototype = {

    preload: function() {
        // We add our `once` methods here so they can
        // be re-initialized when restarting a game
        // w/o requiring a page refresh.
        this.fadeOut = BasicGame.utils.once(function(callback) {
            this.add.tween(BasicGame.music)
                .to({volume: 0}, 2000, Phaser.Easing.Linear.None, true)
                .onComplete.add(function() {
                    BasicGame.music.stop();
                    callback.call(this);
                }, this);
        }, this);
    },
    create: function() {
        var self = this;
        // Use ARCADE physics
        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        // music
        BasicGame.music = this.game.add.audio('stage0', 0, true).play();
        this.add.tween(BasicGame.music).to({volume: 1}, 500, Phaser.Easing.Linear.None, true);

        //  Stage
        this.ground = new BasicGame.Ground(this.game, 0, 0,
            this.game.world.width, this.game.world.height);
        this.game.add.existing(this.ground);

        // Environment - we put this here so the enviroment stuff will render
        // just on top of the ground
        this.environment = new BasicGame.EnvGenerator(this.game).start();

        // Stage -- more
        this.energy = this.game.add.sprite(0, this.game.height, 'energy');
        this.energy.anchor.setTo(0, 1);
        this.energy.height = this.game.height;

        this.fuel = this.game.add.sprite(this.game.width, this.game.height, 'fuel');
        this.fuel.anchor.setTo(1, 1);
        this.fuel.height = this.game.height;
        // this.fuel.angle = 90;

        // this.scoreText = this.game.add.text(16, 16, '', { font: '32px arial', fill: '#fff' });
        // this.scoreText = this.game.add.text(16, 16, '', {font: '14px retro0', fill: '#fff'});
        // this.scoreText = this.game.add.text(16, 16, '', {font: '42px retro1', fill: '#fff'});
        this.scoreText = this.game.add.text(16, 16, '', {font: '16px retro2', fill: '#fff'});
        // the text load via the stylesheet (CSS) so we wait to set the first text
        setTimeout(function() {
            self.scoreText.setText('Score: 0');
        }, 0);
        this.scoreText.setShadow(5, 5, 'rgba(0,0,0,0.5)', 7);
        // update score
        this.scoreTimer = this.game.time.events.loop(Phaser.Timer.SECOND * 0.1, function() {
            if (this.player.health > 0 && this.player.fuel > 0) {
                BasicGame.score += 1;
                this.scoreText.setText('Score: ' + BasicGame.score);
            }
        }, this);
        this.scoreTimer.timer.start();

        // Hero from menu selection
        // this.player = new BasicGame.menuPlayer(this.game,
        //     this.game.world.centerX, this.game.world.centerY+200)._init();
        this.player = new BasicGame.menuPlayer(this.game,
            this.game.world.centerX, this.game.world.centerY+200)._init();
        this.game.add.existing(this.player);

        // Weapons
        this.weakWeapon = new BasicGame.WeaponGenerator(this.game, BasicGame.WeakWeapon);
        this.mediumWeapon = new BasicGame.WeaponGenerator(this.game, BasicGame.MediumWeapon);
        this.strongWeapon = new BasicGame.WeaponGenerator(this.game, BasicGame.StrongWeapon);
        // give the hero a weapon
        this.player.setWeapon(this.weakWeapon);

        // Boosts
        this.fuelBoosts = new BasicGame.BoostGenerator(this.game, BasicGame.FuelBoost);
        this.healthBoosts = new BasicGame.BoostGenerator(this.game, BasicGame.HealthBoost);
        this.mediumWeaponBoosts = new BasicGame.BoostGenerator(this.game, BasicGame.MediumWeaponBoost);
        this.strongWeaponBoosts = new BasicGame.BoostGenerator(this.game, BasicGame.StrongWeaponBoost);

        // fx
        this.explosions = BasicGame.ExplosionGroup.getInstance(this.game, BasicGame.Explosion);

        // Baddies
        this.rocks = new BasicGame.RockGenerator(this.game).start();

        // game.add.button(game.world.centerX - 95, 400, 'button', function() {
        //     console.log('down');
        //     if (game.scale.isFullScreen) {
        //         game.scale.stopFullScreen();
        //     }
        //     else {
        //         game.scale.startFullScreen(false);
        //     }}, this, 2, 1, 0);
        // game.input.onTap.add(function() {
        // game.input.onDown.add(function() {
        //     console.log('down');
        //     if (game.scale.isFullScreen) {
        //         game.scale.stopFullScreen();
        //     }
        //     else {
        //         game.scale.startFullScreen(false);
        //     }
        // }, this);
    },
    collectFuel: function(player, fuelBoost) {
        player.addFuel(fuelBoost.boost);
        fuelBoost.collect();
    },
    collectHealth: function(player, healthBoost) {
        player.addHealth(healthBoost.boost);
        healthBoost.collect();
    },
    collectWeapon: function(player, weaponBoost) {
        var boost = weaponBoost.boost;
        if (boost === 'strong') {
            player.setWeapon(this.strongWeapon);
        } else if (boost === 'medium') {
            player.setWeapon(this.mediumWeapon);
        }
        weaponBoost.collect();
    },
    energyBarUpdate: function() {
        this.energy.height = this.game.height*(this.player.health/this.player.maxHealth);
    },
    fuelBarUpdate: function() {
        this.fuel.height = this.game.height*(this.player.fuel/this.player.maxFuel);
    },
    rockCollision: function(player, rock) {
        player.tint = 0xff0000;
        player.health -= rock.body.mass*2;
        rock.initiateKill();
        player.collide();
    },
    weaponRockCollision: function(weapon, rock) {
        rock.initiateKill();
        weapon.kill();

        // spawn boost ... maybe
        this.fuelBoosts.spawn(rock.position.x, rock.position.y);
        this.healthBoosts.spawn(rock.position.x, rock.position.y);
        this.mediumWeaponBoosts.spawn(rock.position.x, rock.position.y);
        this.strongWeaponBoosts.spawn(rock.position.x, rock.position.y);
    },
    update: function() {
        // baddy collisions
        this.game.physics.arcade.collide(this.player, this.rocks, this.rockCollision, null, this);

        // boost collisions
        this.game.physics.arcade.overlap(this.player, this.fuelBoosts, this.collectFuel, null, this);
        this.game.physics.arcade.overlap(this.player, this.healthBoosts, this.collectHealth, null, this);
        this.game.physics.arcade.overlap(this.player, this.mediumWeaponBoosts, this.collectWeapon, null, this);
        this.game.physics.arcade.overlap(this.player, this.strongWeaponBoosts, this.collectWeapon, null, this);

        // weapon collisions
        this.game.physics.arcade.overlap(this.weakWeapon, this.rocks, this.weaponRockCollision, null, this);
        this.game.physics.arcade.overlap(this.mediumWeapon, this.rocks, this.weaponRockCollision, null, this);
        this.game.physics.arcade.overlap(this.strongWeapon, this.rocks, this.weaponRockCollision, null, this);

        // assest updates
        this.energyBarUpdate();
        this.fuelBarUpdate();

        // check for death
        if (this.player.fuel <= 0 || this.player.health <= 0) {
            this.player.kill();
        }

        if (!this.player.alive) {
            this.gameOver();
        }
    },
    gameOver: function() {
        this.fadeOut(function() {
            this.game.state.start('GameOver');
        });
    },
    quitGame: function() {
        this.fadeOut(function() {
            this.game.state.start('MainMenu');
        });
    },
    shutdown: function() {
        this.ground.destroy();
        this.fuel.destroy();
        this.energy.destroy();

        this.scoreText.destroy();

        this.player.destroy();

        this.rocks.destroy();

        this.fuelBoosts.destroy();
        this.healthBoosts.destroy();
        this.mediumWeaponBoosts.destroy();
        this.strongWeaponBoosts.destroy();

        this.weakWeapon.destroy();
        this.mediumWeapon.destroy();
        this.strongWeapon.destroy();

        this.explosions.destroy();
    }

};
