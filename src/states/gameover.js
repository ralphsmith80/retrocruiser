
BasicGame.GameOver = function(game) {
};

BasicGame.GameOver.prototype = {

    preload: function() {
    },
    create: function() {
        var text = this.game.add.text(this.game.world.centerX, 50, //this.game.world.centerY,
            'Game Over', {font: '16px retro2', fill: '#fff'});
        text.anchor.setTo(0.5, 0);

        BasicGame.music = this.game.add.audio('game_over1', 1, true).play();

        this.game.input.onDown.addOnce(function() {
            this.add.tween(this.group)
                .to({alpha: 0}, 2000, Phaser.Easing.Linear.None, true)
                .onComplete.add(this.startGame, this);
            this.add.tween(BasicGame.music)
                .to({volume: 0}, 2000, Phaser.Easing.Linear.None, true);
        }, this);

        var scoreboard = new BasicGame.Scoreboard(this.game);
        this.game.add.existing(scoreboard);
        // set score to show
        scoreboard.show(BasicGame.score);

        // emitter effect
        // @params (game, x, y, maxParticles)
        emitter = game.add.emitter(game.world.centerX, game.world.height, 200);
        emitter.makeParticles('corona');
        // emitter.makeParticles('bubble');
        emitter.setRotation(0, 0);
        emitter.setAlpha(0.3, 0.8);
        emitter.setScale(0.5, 1);
        emitter.gravity = -200;
        // emitter.setXSpeed(0,0);
        emitter.setYSpeed(0,0);
        emitter.area.width = game.world.width;

        //  false means don't explode all the sprites at once, but instead release at a rate of one particle per 50ms
        //  The 5000 value is the lifespan of each particle before it's killed
        emitter.start(false, 5000, 50);

        this.group = this.game.add.group();
        this.group.add(text);
        this.group.add(scoreboard);
        this.group.add(emitter);
    },
    startGame: function() {
        BasicGame.music.stop();
        this.game.state.start('MainMenu');
        BasicGame.score = 0;
        // this.game.state.start('Preloader');
        BasicGame.music = this.game.add.audio('title_sound', 1, true);
        BasicGame.music.play();
    },
    update: function() {
    }
};
