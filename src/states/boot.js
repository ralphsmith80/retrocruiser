BasicGame = {

    /* Here we've just got some global level vars that persist regardless of State swaps */
    score: 0,

    /* If the music in your game needs to play through-out a few State swaps, then you could reference it here */
    music: null,

    /* Your game can check BasicGame.orientated in internal loops to know if it should pause or not */
    orientated: false,

    // player selected on main menu
    menuPlayer: null

};

BasicGame.Boot = function(game) {
};

BasicGame.Boot.prototype = {

    preload: function() {
        //  Here we load the assets required for our preloader (in this case a background and a loading bar)
        this.game.load.image('title', 'assets/images/boot/title.gif');
        this.game.load.image('loading', 'assets/images/boot/preloader_bar.png');
    },

    create: function() {
        //  Unless you specifically know your game needs to support multi-touch I would recommend setting this to 1
        this.game.input.maxPointers = 2;

        //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
        this.game.disableVisibilityChange = true;

        if (this.game.device.desktop) {
            //  If you have any desktop specific settings, they can go in here
            this.game.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.game.scale.minWidth = SAFE_ZONE_WIDTH;
            this.game.scale.minHeight = SAFE_ZONE_HEIGHT;
            this.game.scale.maxWidth = 2560;
            this.game.scale.maxHeight = 1440;
            // this.game.scale.minWidth = 480;
            // this.game.scale.minHeight = 260;
            // this.game.scale.maxWidth = 1024;
            // this.game.scale.maxHeight = 768;
            this.game.scale.pageAlignHorizontally = true;
            this.game.scale.pageAlignVertically = true;
            this.game.scale.setScreenSize(true);
        }
        else {
            //  Same goes for mobile settings.
            //  In this case we're saying "scale the game, no lower than 480x320 and no higher than 1024x768"
            this.game.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.game.scale.minWidth = SAFE_ZONE_WIDTH;
            this.game.scale.minHeight = SAFE_ZONE_HEIGHT;
            this.game.scale.maxWidth = 2560;
            this.game.scale.maxHeight = 1440;
            // this.game.scale.minWidth = 480;
            // this.game.scale.minHeight = 260;
            // this.game.scale.maxWidth = 1024;
            // this.game.scale.maxHeight = 768;
            this.game.scale.pageAlignHorizontally = true;
            this.game.scale.pageAlignVertically = true;
            // @params (forceLandscape, forcePortrait, orientationImage)
            this.game.scale.forceOrientation(false, false);
            this.game.scale.hasResized.add(this.gameResized, this);
            this.game.scale.enterIncorrectOrientation.add(this.enterIncorrectOrientation, this);
            this.game.scale.leaveIncorrectOrientation.add(this.leaveIncorrectOrientation, this);
            this.game.scale.setScreenSize(true);
        }

        // scale screen
        this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
        this.game.scale.setShowAll();
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVeritcally = true;
        this.game.scale.setScreenSize(true);
        this.game.scale.refresh();

        window.addEventListener('resize', function(event){
            game.scale.setShowAll();
            game.scale.refresh();
        });

        //  By this point the preloader assets have loaded to the cache, we've set the game settings
        //  So now let's start the real preloader going
        this.game.state.start('Preloader');
    },

    gameResized: function(width, height) {
        //  This could be handy if you need to do any extra processing if the game resizes.
        //  A resize could happen if for example swapping orientation on a device.
    },

    enterIncorrectOrientation: function() {
        BasicGame.orientated = false;
        document.getElementById('orientation').style.display = 'block';
    },

    leaveIncorrectOrientation: function() {
        BasicGame.orientated = true;
        document.getElementById('orientation').style.display = 'none';
    }

};
