BasicGame.MainMenu = function(game) {
    // 2 second fade
    this.fadeDuration = 2000;
    this.menu = null;
};

BasicGame.MainMenu.prototype = {

    create: function() {
        var selectMenuY = this.game.world.height - 100;

        this.menu = this.game.add.group();

        this.text = this.game.add.text(game.world.centerX, game.world.centerY,
            'SELECT YOUR HERO', {font: '16px retro2', fill: '#fff'});
        this.text.anchor.setTo(0.5, 0.5);

        var background, heroGroup;
        // background = this.add.sprite(0, 0, 'ground');
        background = this.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'ground');
        background.width = this.game.width;
        background.height = this.game.height;

        heroGroup = new BasicGame.HeroGroup(this.game);
        // heroGroup.position.setTo(this.game.world.centerX - (heroGroup.width/2), selectMenuY);
        heroGroup.position.setTo(this.game.world.centerX - (heroGroup.width/2) + 35, selectMenuY);

        this.menu.add(background);
        this.menu.add(heroGroup);

        heroGroup.onSelected(function(hero) {
            heroGroup.offSelected();
            BasicGame.menuPlayer = hero.class;
            this.startGame();
        }, this);

        // virtual button input
        // var startX = this.game.world.centerX,
        //     deltaX = heroGroup.width;
        // var leftButton, rightButton, startButton;
        // leftButton = this.game.add.button(startX-deltaX, selectMenuY, 'left-arrow',
        //     null, this, 1, 0, 1, 0);
        // leftButton.anchor.setTo(0.5, 0.5);
        // rightButton = this.game.add.button(startX+deltaX, selectMenuY, 'right-arrow',
        //     null, this, 1, 0, 1, 0);
        // rightButton.anchor.setTo(0.5, 0.5);
        // startButton = this.game.add.button(startX, selectMenuY+heroGroup.height, 'horizontal',
        //     null, this, 1, 0, 1, 0);
        // startButton.anchor.setTo(0.5, 0.5);

        // - rotate herogroup for selection
        // leftButton.events.onInputDown.add(heroGroup.rotateLeft, heroGroup);
        // rightButton.events.onInputDown.add(heroGroup.rotateRight, heroGroup);
        // - slide herogroup for selection
        // leftButton.events.onInputDown.add(heroGroup.shiftRight, heroGroup);
        // rightButton.events.onInputDown.add(heroGroup.shiftLeft, heroGroup);
        // startButton.events.onInputDown.add(start, this);

        // keyboard input
        // var leftKey, rightKey, enterKey;
        // leftKey = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        // rightKey = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        // enterKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        // leftKey.onDown.add(heroGroup.rotateLeft, heroGroup);
        // rightKey.onDown.add(heroGroup.rotateRight, heroGroup);
        // leftKey.onDown.add(heroGroup.shiftLeft, heroGroup);
        // rightKey.onDown.add(heroGroup.shiftRight, heroGroup);
        // enterKey.onDown.add(start, this);

        // fade in
        this.game.add.tween(this.menu).from({alpha: 0}, this.fadeDuration, Phaser.Easing.Linear.None, true);
        this.game.add.tween(this.text).from({alpha: 0}, this.fadeDuration, Phaser.Easing.Linear.None, true);
        // this.game.add.tween(BasicGame.music).to({volume: 1}, this.fadeDuration, Phaser.Easing.Linear.None, true);

        // function start() {
        //     // can't change selection
        //     leftKey.enabled = false;
        //     rightKey.enabled = false;
        //     // set selected player
        //     BasicGame.menuPlayer = heroGroup.cursor.class;
        //     this.startGame();
        // }
        // this.game.state.start('Game');
    },
    update: function() {
    },
    startGame: function() {
        var tween = this.add.tween(this.menu).to({alpha: 0}, this.fadeDuration, Phaser.Easing.Linear.None, true);
        tween.onComplete.add(function() {
            this.game.state.start('Game');
            BasicGame.music.stop();
        }, this);
        // fade out text
        this.add.tween(this.text).to({alpha: 0}, this.fadeDuration, Phaser.Easing.Linear.None, true);
        // fade out music
        this.game.add.tween(BasicGame.music).to({volume: 0}, this.fadeDuration, Phaser.Easing.Linear.None, true);
    }
};
