BasicGame.Bush = function(game, x, y, frame) {
    var name = 'bush';
    BasicGame.Environment.call(this, game, x, y, name, frame);
};
BasicGame.Bush.prototype = Object.create(BasicGame.Environment.prototype);
BasicGame.Bush.constructor = BasicGame.Bush;
BasicGame.Bush.prototype._init = function() {
    this.body.velocity.setTo(0, 50);
    return this;
};
