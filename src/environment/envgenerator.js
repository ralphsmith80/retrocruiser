BasicGame.EnvGenerator = function(game) {
    // we're don't pass in a base sprite because the
    // spawn behavior will randomly select an environment class and spawn that.
    BasicGame.RecycleGroup.call(this, game);
};
BasicGame.EnvGenerator.prototype = Object.create(BasicGame.RecycleGroup.prototype);
BasicGame.EnvGenerator.constructor = BasicGame.EnvGenerator;
BasicGame.EnvGenerator.prototype.spawn = function() {
    // pick a random environment class and spawn that at the top
    // of the screen at a random 'x' location
    var x = this.game.rnd.integerInRange(0, this.game.width),
        y = -50,
        classes = [
            BasicGame.Bush
        ],
        random = this.game.rnd.integerInRange(0, 0),
        Bush = classes[random],
        sprite;
    this.Sprite = Bush;
    return BasicGame.RecycleGroup.prototype.spawn.call(this, x, y);
};
BasicGame.EnvGenerator.prototype.start = function() {
    this.generator = this.game.time.events.loop(Phaser.Timer.SECOND * 2, this.spawn, this);
    this.generator.timer.start();
    return this;
};
