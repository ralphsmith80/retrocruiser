// environment base class/interface
BasicGame.Environment = function(game, x, y, name, frame) {
    Phaser.Sprite.call(this, game, x, y, name, frame);
    this.name = name;
    this.anchor.setTo(0.5, 0.5);

    this.game.physics.arcade.enableBody(this);
    this.checkWorldBounds = true;
    this.outOfBoundsKill = true;
};
BasicGame.Environment.prototype = Object.create(Phaser.Sprite.prototype);
BasicGame.Environment.constructor = BasicGame.Environment;
BasicGame.Environment.prototype._init = function() {
    return this;
};
BasicGame.Environment.prototype.reset = function() {
    Phaser.Sprite.prototype.reset.apply(this, arguments);
    this._init();
    return this;
};
