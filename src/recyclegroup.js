BasicGame.RecycleGroup = function(game, Sprite, parent) {
    Phaser.Group.call(this, game, parent);
    this.Sprite = Sprite;
};
BasicGame.RecycleGroup.prototype = Object.create(Phaser.Group.prototype);
BasicGame.RecycleGroup.constructor = BasicGame.RecycleGroup;

BasicGame.RecycleGroup.prototype.spawn = function(x, y) {
    var sprite = this.getFirstExists(false);
    if (!sprite) {
        sprite = new this.Sprite(this.game, x, y);
        this.add(sprite);
    }
    sprite.reset(x, y);
    return sprite;
};
