var w = window.innerWidth ;//* pixelRatio,
    h = window.innerHeight;//* pixelRatio;

const SAFE_ZONE_WIDTH = 320;
// set the safe zone to for the proper aspect ratio for smaller screens
// - iPhone 5
// - iPhone 6
// - default 480
const SAFE_ZONE_HEIGHT = (h < 480) ? h : 480;

var lw, lh; //landscape width/height in pixels
if ( h > w ) {
    lw = h;
    lh = w;
} else {
    lw = w;
    lh = h;
}
var aspectRatioDevice = lw/lh;

var aspectRatioSafeZone = SAFE_ZONE_WIDTH / SAFE_ZONE_HEIGHT;
var extraWidth = 0, extraHeight = 0;
if (aspectRatioSafeZone < aspectRatioDevice) {
    // have to add game pixels vertically in order to fill the device screen
    extraWidth = aspectRatioDevice * SAFE_ZONE_HEIGHT - SAFE_ZONE_WIDTH;
} else {
    // have to add game pixels horizontally
    extraHeight = SAFE_ZONE_WIDTH / aspectRatioDevice - SAFE_ZONE_HEIGHT;
}

var game = new Phaser.Game( SAFE_ZONE_WIDTH + extraWidth, SAFE_ZONE_HEIGHT + extraHeight, Phaser.AUTO, 'game');

game.state.add('Boot', BasicGame.Boot);
game.state.add('Preloader', BasicGame.Preloader);
game.state.add('MainMenu', BasicGame.MainMenu);
game.state.add('Game', BasicGame.Game);
game.state.add('GameOver', BasicGame.GameOver);

game.state.start('Boot');
