BasicGame.Controller = function(game) {
    var name = 'controller';
    this.name = name;
    this.buttons = {
        left: game.add.group(),
        right: game.add.group()
    };

    var center,
        left,
        right,
        up,
        down,
        strifeUL,
        strifeUR,
        strifeDL,
        strifeDR,
        fire;

    var viewY           = game.height,
        viewX           = game.width,
        buttonWidth     = 48,
        horizontalBtnX  = buttonWidth,
        horizontalBtnY  = viewY - (buttonWidth*2),
        verticalBtnX    = buttonWidth*2,
        verticalBtnY    = horizontalBtnY - buttonWidth,
        fireBtnWidth    = 64;

    // left side
    left = game.add.button(horizontalBtnX, horizontalBtnY, 'horizontal', null, this, 1, 0, 1, 0);
    left.anchor.setTo(0.5, 0.5);
    left.fixedToCamera = true;
    left.events.onInputOver.add(function() {this.left=true;}, this);
    left.events.onInputOut.add(function() {this.left=false;}, this);
    left.events.onInputDown.add(function() {this.left=true;}, this);
    left.events.onInputUp.add(function() {this.left=false;}, this);

    right = game.add.button(horizontalBtnX+left.width, horizontalBtnY, 'horizontal', null, this, 1, 0, 1, 0);
    right.anchor.setTo(0.5, 0.5);
    right.fixedToCamera = true;
    right.events.onInputOver.add(function() {this.right=true;}, this);
    right.events.onInputOut.add(function() {this.right=false;}, this);
    right.events.onInputDown.add(function() {this.right=true;}, this);
    right.events.onInputUp.add(function() {this.right=false;}, this);

    up = game.add.button(verticalBtnX, verticalBtnY, 'vertical', null, this, 1, 0, 1, 0);
    up.anchor.setTo(0.5, 0.5);
    up.fixedToCamera = true;
    up.events.onInputOver.add(function() {this.up=true;}, this);
    up.events.onInputOut.add(function() {this.up=false;}, this);
    up.events.onInputDown.add(function() {this.up=true;}, this);
    up.events.onInputUp.add(function() {this.up=false;}, this);

    down = game.add.button(verticalBtnX, verticalBtnY+up.height, 'vertical', null, this, 1, 0, 1, 0);
    down.anchor.setTo(0.5, 0.5);
    down.fixedToCamera = true;
    down.events.onInputOver.add(function() {this.down=true;}, this);
    down.events.onInputOut.add(function() {this.down=false;}, this);
    down.events.onInputDown.add(function() {this.down=true;}, this);
    down.events.onInputUp.add(function() {this.down=false;}, this);

    strifeUL = game.add.button(buttonWidth*0.75, horizontalBtnY - buttonWidth*1.25, 'buttondiagonal', null, this, 2, 0, 2, 0);
    strifeUL.anchor.setTo(0.5, 0.5);
    strifeUL.fixedToCamera = true;
    strifeUL.events.onInputOver.add(function() {this.left=true; this.up=true;}, this);
    strifeUL.events.onInputOut.add(function() {this.left=false; this.up=false;}, this);
    strifeUL.events.onInputDown.add(function() {this.left=true; this.up=true;}, this);
    strifeUL.events.onInputUp.add(function() {this.left=false; this.up=false;}, this);

    strifeUR = game.add.button(buttonWidth*3.25, horizontalBtnY - buttonWidth*1.25, 'buttondiagonal', null, this, 3, 1, 3, 1);
    strifeUR.anchor.setTo(0.5, 0.5);
    strifeUR.fixedToCamera = true;
    strifeUR.events.onInputOver.add(function() {this.right=true; this.up=true;}, this);
    strifeUR.events.onInputOut.add(function() {this.right=false; this.up=false;}, this);
    strifeUR.events.onInputDown.add(function() {this.right=true; this.up=true;}, this);
    strifeUR.events.onInputUp.add(function() {this.right=false; this.up=false;}, this);

    strifeDL = game.add.button(buttonWidth*0.75, horizontalBtnY + buttonWidth*1.25, 'buttondiagonal', null, this, 6, 4, 6, 4);
    strifeDL.anchor.setTo(0.5, 0.5);
    strifeDL.fixedToCamera = true;
    strifeDL.events.onInputOver.add(function() {this.left=true; this.down=true;}, this);
    strifeDL.events.onInputOut.add(function() {this.left=false; this.down=false;}, this);
    strifeDL.events.onInputDown.add(function() {this.left=true; this.down=true;}, this);
    strifeDL.events.onInputUp.add(function() {this.left=false; this.down=false;}, this);

    strifeDR = game.add.button(buttonWidth*3.25, horizontalBtnY + buttonWidth*1.25, 'buttondiagonal', null, this, 7, 5, 7, 5);
    strifeDR.anchor.setTo(0.5, 0.5);
    strifeDR.fixedToCamera = true;
    strifeDR.events.onInputOver.add(function() {this.right=true; this.down=true;}, this);
    strifeDR.events.onInputOut.add(function() {this.right=false; this.down=false;}, this);
    strifeDR.events.onInputDown.add(function() {this.right=true; this.down=true;}, this);
    strifeDR.events.onInputUp.add(function() {this.right=false; this.down=false;}, this);

    // this button is just a place holder to proved a finger rest in the middle of the group
    // i.e. if you finger is in the middle no movement will occure
    // center = game.add.button(horizontalBtnX+buttonWidth, horizontalBtnY, 'buttonfire', null, this, 0, 0, 0, 0);
    // center.anchor.setTo(0.5, 0.5);
    center = game.add.button(horizontalBtnX+buttonWidth/2, horizontalBtnY-buttonWidth/2, 'buttonfire', null, this, 0, 0, 0, 0);
    center.anchor.setTo(0.25, 0.25);
    center.fixedToCamera = true;
    center.events.onInputOver.add(function() {
        this.left   = false;
        this.right  = false;
        this.up     = false;
        this.down   = false;
    }, this);
    center.events.onInputDown.add(function() {
        this.left   = false;
        this.right  = false;
        this.up     = false;
        this.down   = false;
    }, this);

    // right side
    // fire = game.add.button(viewX - fireBtnWidth, horizontalBtnY, 'buttonfire', null, this, 1, 0, 1, 0);
    fire = game.add.button(viewX, horizontalBtnY, 'buttonfire', null, this, 1, 0, 1, 0);
    fire.anchor.setTo(0.5, 0.5);
    fire.fixedToCamera = true;
    fire.events.onInputDown.add(function() {this.fire=true;}, this);
    fire.events.onInputUp.add(function() {this.fire=false;}, this);


    this.buttonLeft  = left;
    this.buttonRight = right;
    this.buttonUp    = up;
    this.buttonDown  = down;
    this.buttonStrifeUL = strifeUL;
    this.buttonStrifeUR = strifeUR;
    this.buttonStrifeDL = strifeDL;
    this.buttonStrifeDR = strifeDR;


    this.fire  = fire;
    this.left   = false;
    this.right  = false;
    this.up     = false;
    this.down   = false;
    this.fire   = false;

    // left button group
    this.buttons.left.add(left);
    this.buttons.left.add(right);
    this.buttons.left.add(up);
    this.buttons.left.add(down);
    this.buttons.left.add(strifeUL);
    this.buttons.left.add(strifeUR);
    this.buttons.left.add(strifeDL);
    this.buttons.left.add(strifeDR);
    this.buttons.left.add(center);

    // postion and scale left button group
    this.buttons.left.scale.set(0.5, 0.5);
    this.buttons.left.position.setTo(10, game.height/2-10);

    // right button group
    this.buttons.right.add(fire);
    this.buttons.right.scale.set(0.5, 0.5);
    this.buttons.right.position.setTo((game.width/2 - fireBtnWidth/2)-10, game.height/2-10);
};
