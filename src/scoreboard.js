BasicGame.Scoreboard = function(game) {
    Phaser.Group.call(this, game);

    this.currentScoreText = this.game.add.text(game.world.centerX, 100, '', {font: '16px retro2', fill: '#3AB2F7'});
    this.currentScoreText.anchor.setTo(0.5, 0);
    this.currentScoreText.scale.setTo(0.75, 0.75);

    this.scoreText = this.game.add.text(game.world.centerX, 150, '', {font: '16px retro2', fill: '#fff'});
    this.scoreText.anchor.setTo(0.5, 0);
    this.scoreText.scale.setTo(0.75, 0.75);

    this.add(this.currentScoreText);
    this.add(this.scoreText);

    // set alpha to 0 so we can fade in on show
    this.alpha = 0;

    return this;
};
BasicGame.Scoreboard.prototype = Object.create(Phaser.Group.prototype);
BasicGame.Scoreboard.constructor = BasicGame.Scoreboard;

BasicGame.Scoreboard.prototype.getScores = function() {
    return JSON.parse(localStorage.getItem('scores'));
};

BasicGame.Scoreboard.prototype.saveScores = function(scores) {
    localStorage.setItem('scores', JSON.stringify(scores));
};

BasicGame.Scoreboard.prototype.show = function(score) {
    var self = this,
        newHighScore = false,
        oldHighScore = 0,
        number = 1,
        scores, currentScore, print = [];

    currentScore = {
        date: new Date(),
        value: score
    };

    if(!!localStorage) {
        scores = this.getScores() || [];
        scores = scores.concat(currentScore);
        scores.sort(function(a, b) {
            return b.value - a.value;
        }).forEach(function(score) {
            if (score.value > oldHighScore) {
                oldHighScore = score.value;
            }
            // self.scoreText.setText('\n' + score.date.toLocaleString() + ' -----> ' + score.value);
            var date = new Date(score.date);
            print.push(number++ +'. '+date.toLocaleString() + ' -----> ' + score.value);
        });


        // new best score
        if(currentScore.value >= oldHighScore) {
           newHighScore = true;
        }
    } else {
        // Fallback. LocalStorage isn't available
        this.scoreText.setText('Unable to save scores. Try a newer browser');
    }

    if (newHighScore) {
        this.currentScoreText.setText('NEW HIGH SCORE!!!\nYOUR SCORE: ' + currentScore.value);
    } else {
        this.currentScoreText.setText('YOUR SCORE: ' + currentScore.value);
    }

    this.scoreText.setText(print.join('\n'));

    // only save 10 scores at most
    scores.splice(9, scores.length);
    this.saveScores(scores);

    this.game.add.tween(this).to({alpha: 1}, 2000, Phaser.Easing.Linear.None, true);
};
