BasicGame.utils = {
    clamp: function(value, min, max) {
        var ret = Math.min(Math.max(value, min), max);
        if (isNaN(ret)) {
            throw Error('"value" must be of type "number" recieved type: "' + typeof value + '"');
        }
        return ret;
    },
    once: function(fn, context) {
        var result;
        return function() {
            if(fn) {
                result = fn.apply(context || this, arguments);
                fn = null;
            }
            return result;
        };
    }
};
