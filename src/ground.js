
BasicGame.Ground = function(game, x, y, width, height, key, frame) {
    var key = 'ground';
    Phaser.TileSprite.call(this, game, x, y, width, height, key, frame);

    // cursors
    this.cursors = this.game.input.keyboard.createCursorKeys();

    this.autoScroll(0, 50);
};
BasicGame.Ground.prototype = Object.create(Phaser.TileSprite.prototype);
BasicGame.Ground.constructor = BasicGame.Ground;

// BasicGame.Ground.prototype.update = function() {
//     if (this.cursors.left.isDown) {
//         this.tilePosition.x += 6;
//     } else if (this.cursors.right.isDown) {
//         this.tilePosition.x -= 6;
//     }
//     if (this.cursors.up.isDown) {
//         this.tilePosition.y += 6;
//     }
//     else if (this.cursors.down.isDown) {
//         this.tilePosition.y -= 6;
//     }
// };
