BasicGame.ExplosionGroup = function(game, Sprite) {
    BasicGame.RecycleGroup.call(this, game, Sprite);
};
BasicGame.ExplosionGroup.prototype = Object.create(BasicGame.RecycleGroup.prototype);
BasicGame.ExplosionGroup.constructor = BasicGame.ExplosionGroup;

BasicGame.ExplosionGroup.prototype.destroy = function() {
    BasicGame.ExplosionGroup.instance = null;
    BasicGame.RecycleGroup.prototype.destroy.apply(this, arguments);
};
BasicGame.ExplosionGroup.prototype.spawn = function(x, y) {
    var explosion = BasicGame.RecycleGroup.prototype.spawn.call(this, x, y);
    explosion.restart();
};

// make a singleton method
// We will be using a lot of explosions so rather than
// manage many groups of explosions we're going to provide
// a singleton so we can share the explosion group.
// Note that were passing in BasicGame.ExplosionGroup in as this context.
// This allows use to reset this group for re-init when in the ExplosionGroup.destroy method
BasicGame.ExplosionGroup.getInstance = (function() {
    // var instance;
    return function(game, Sprite) {
        if (!this.instance) {
            this.instance = new BasicGame.ExplosionGroup(game, Sprite);
        }
        return this.instance;
    };
}).call(BasicGame.ExplosionGroup);


BasicGame.Explosion = function(game, x, y, frame) {
    var frameRate = 8,
        loop = false,
        killOnComplete = true;

    Phaser.Sprite.call(this, game, x, y, 'explode', frame);
    this.anchor.setTo(0.5, 0.5);
    this.animations.add('explosion');
    this.animations.play('explosion', frameRate, loop, killOnComplete);
    this.name = 'explosion';


    // check if explosion is out of bounds
    // if so kill sprite
    // the animation will kill it self but no need to wait if it off screen
    this.checkWorldBounds = true;
    this.outOfBoundsKill = true;
};
BasicGame.Explosion.prototype = Object.create(Phaser.Sprite.prototype);
BasicGame.Explosion.constructor = BasicGame.Explosion;

BasicGame.Explosion.prototype.onComplete = function(callback, context) {
    var animation = this.animations.getAnimation('explosion');
    animation.onComplete.add(callback, context);
};
BasicGame.Explosion.prototype.restart = function() {
    var animation = this.animations.getAnimation('explosion');
    // animation.setFrame(1, true);
    animation.restart();
};
