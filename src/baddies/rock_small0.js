BasicGame.RockSmall_0 = function(game, x, y, frame) {
    var name = 'rock4';
    BasicGame.Rock.call(this, game, x, y, name, frame);
};
BasicGame.RockSmall_0.prototype = Object.create(BasicGame.Rock.prototype);
BasicGame.RockSmall_0.constructor = BasicGame.RockSmall_0;
BasicGame.RockSmall_0.prototype._init = function() {
    var velocityY = this.game.rnd.integerInRange(50, 200);
    this.body.velocity.setTo(0, velocityY);
    this.body.mass = 2;
    return this;
};
