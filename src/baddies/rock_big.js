BasicGame.RockBig = function(game, x, y, frame) {
    var name = 'rock0';
    BasicGame.Rock.call(this, game, x, y, name, frame);
};
BasicGame.RockBig.prototype = Object.create(BasicGame.Rock.prototype);
BasicGame.RockBig.constructor = BasicGame.RockBig;
BasicGame.RockBig.prototype._init = function() {
    var velocityY = this.game.rnd.integerInRange(50, 200);
    this.body.velocity.setTo(0, velocityY);
    this.body.mass = 5;
    return this;
};
