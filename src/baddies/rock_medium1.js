BasicGame.RockMedium_1 = function(game, x, y, frame) {
    var name = 'rock1';
    BasicGame.Rock.call(this, game, x, y, name, frame);
};
BasicGame.RockMedium_1.prototype = Object.create(BasicGame.Rock.prototype);
BasicGame.RockMedium_1.constructor = BasicGame.RockMedium_1;
BasicGame.RockMedium_1.prototype._init = function() {
    var velocityY = this.game.rnd.integerInRange(50, 200);
    this.body.velocity.setTo(0, velocityY);
    this.body.mass = 3;
    return this;
};
