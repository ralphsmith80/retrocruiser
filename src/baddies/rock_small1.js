BasicGame.RockSmall_1 = function(game, x, y, frame) {
    var name = 'rock3';
    BasicGame.Rock.call(this, game, x, y, name, frame);
};
BasicGame.RockSmall_1.prototype = Object.create(BasicGame.Rock.prototype);
BasicGame.RockSmall_1.constructor = BasicGame.RockSmall_1;
BasicGame.RockSmall_1.prototype._init = function() {
    var velocityY = this.game.rnd.integerInRange(50, 200);
    this.body.velocity.setTo(0, velocityY);
    this.body.mass = 2;
    return this;
};
