// rock base class/interface
BasicGame.Rock = function(game, x, y, name, frame) {
    Phaser.Sprite.call(this, game, x, y, name, frame);
    this.name = name;
    this.anchor.setTo(0.5, 0.5);

    this.game.physics.arcade.enableBody(this);
    this.body.minBounceVelocity = 0;
    this.checkWorldBounds = true;
    this.outOfBoundsKill = true;

    // All baddies explode the same way when they die
    // We could create explode/kill behaviors but we've already demonstraited
    // how to do that and it this case every baddy just explodes.
    this.explosions = BasicGame.ExplosionGroup.getInstance(this.game, BasicGame.Explosion);

    // sound
    this.sound = this.game.add.audio('explosion2', 1);
};
BasicGame.Rock.prototype = Object.create(Phaser.Sprite.prototype);
BasicGame.Rock.constructor = BasicGame.Rock;
BasicGame.Rock.prototype._init = function() {
    return this;
};
// We want a pre-kill method rather than just overriding the kill method.
// If we were to just spawn an explosion in the base kill method
// then you would see explosion when an baddies dies because it's out of bounds.
BasicGame.Rock.prototype.initiateKill = function() {
    this.explosions.spawn(this.position.x, this.position.y);
    this.sound.play();
    this.kill();
};
BasicGame.Rock.prototype.reset = function() {
    Phaser.Sprite.prototype.reset.apply(this, arguments);
    this._init();
    return this;
};
