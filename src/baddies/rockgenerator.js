BasicGame.RockGenerator = function(game) {
    // we're don't pass in a base sprite because the
    // spawn behavior will randomly select a rock class and spawn that.
    BasicGame.RecycleGroup.call(this, game);
};
BasicGame.RockGenerator.prototype = Object.create(BasicGame.RecycleGroup.prototype);
BasicGame.RockGenerator.constructor = BasicGame.RockGenerator;
BasicGame.RockGenerator.prototype.spawn = function() {
    // pick a random rock class and spawn that at the top
    // of the screen at a random 'x' location
    var x = this.game.rnd.integerInRange(0, this.game.width),
        y = 0,
        classes = [
            BasicGame.RockSmall_0,
            BasicGame.RockSmall_1,
            BasicGame.RockMedium_0,
            BasicGame.RockMedium_1,
            BasicGame.RockBig
        ],
        random = game.rnd.integerInRange(0, 4),
        Rock = classes[random],
        sprite;
    this.Sprite = Rock;
    return BasicGame.RecycleGroup.prototype.spawn.call(this, x, y);
};

BasicGame.RockGenerator.prototype.start = function() {
    this.generator = this.game.time.events.loop(Phaser.Timer.SECOND * 0.5, this.spawn, this);
    this.generator.timer.start();
    return this;
};
