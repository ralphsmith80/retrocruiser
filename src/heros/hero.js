BasicGame.Hero = function(game, x, y, name) {
    Phaser.Sprite.apply(this, arguments);
    this.name = name;

    this.game.physics.arcade.enableBody(this);
    this.anchor.setTo(0.5, 0.5);

    this.weapon = null;

    this.body.minBounceVelocity = 0;
    this.body.maxVelocity.set(200);
    this.body.drag.set('200');

    this.body.collideWorldBounds = true;

    // fx
    this.baseTint = this.tint;

    // cursors
    this.cursors = this.game.input.keyboard.createCursorKeys();
    var leftButton = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
    leftButton.onDown.add(this.onLeft, this);
    var rightButton = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
    rightButton.onDown.add(this.onRight, this);

    // virtual controller
    this.controller = new BasicGame.Controller(this.game);
    this.controller.buttonLeft.events.onInputDown.add(this.onLeft, this);
    this.controller.buttonLeft.events.onInputOver.add(this.onLeft, this);
    this.controller.buttonRight.events.onInputDown.add(this.onRight, this);
    this.controller.buttonRight.events.onInputOver.add(this.onRight, this);

    this.controller.buttonStrifeUL.events.onInputDown.add(this.onLeft, this);
    this.controller.buttonStrifeUL.events.onInputOver.add(this.onLeft, this);
    this.controller.buttonStrifeUR.events.onInputDown.add(this.onRight, this);
    this.controller.buttonStrifeUR.events.onInputOver.add(this.onRight, this);
    this.controller.buttonStrifeDL.events.onInputDown.add(this.onLeft, this);
    this.controller.buttonStrifeDL.events.onInputOver.add(this.onLeft, this);
    this.controller.buttonStrifeDR.events.onInputDown.add(this.onRight, this);
    this.controller.buttonStrifeDR.events.onInputOver.add(this.onRight, this);

    // hide virtual controls on desktop
    // we could be smarter an not bind events an all that but if the mobile platform
    // can handle it then it shouldn't be a problem for the desktop
    if (this.game.device.desktop) {
        this.controller.buttons.left.visible = false;
        this.controller.buttons.right.visible = false;
    }

    // sound
    this.sound = {
        die: this.game.add.audio('die', 1),
        // thrust: this.game.add.audio('thrust0', 1, true)
        // thrust: this.game.add.audio('thrust1', 1, true)
        thrust: this.game.add.audio('thrust2', 1, true),
        // thrust: this.game.add.audio('thrust3', 1, true)
        // thrust: this.game.add.audio('thrust4', 1, true)
        fall: this.game.add.audio('fall0', 1, false),
        // fall: this.game.add.audio('fall1', 1, false)
        alarm: this.game.add.audio('alarm', 1, false)
    };
    this.sound.thrust.play();
};
BasicGame.Hero.prototype = Object.create(Phaser.Sprite.prototype);
BasicGame.Hero.constructor = BasicGame.Hero;

BasicGame.Hero.prototype._init = function() {
    var self = this;
    this.speed = 100;
    this.maxHealth = 100;
    this.health = 100;
    this.maxFuel = 100;
    this.fuel = 100;
    this.fuelCost = {
        idle: 0.01,
        move: 0.05
    };
    // Our `once` methods that can happen only once per game need to
    // be reset in the init method so they will execute again in the next game
    // w/o requiring a page refresh.
    // can only fall once
    this.fall = BasicGame.utils.once(this._fall, this);
    // can only be killed once
    this.kill = BasicGame.utils.once(function() {
        if (this.fuel <= 0) {
            this.fall(this._kill);
        } else if (this.health <= 0) {
            this._kill();
        } else {
            console.warn('why you kill me!');
            this._kill();
        }
        return this;
    }, this);
    return this;
};
BasicGame.Hero.prototype._fall = function(onComplete) {
    var self = this,
        // we set the scale factor to apply to each hero so all
        // heros will fall at the same rate
        scaleFactor = 0.1 * this.scale.x,
        i = setInterval(function() {
            if (self.scale.y <= scaleFactor) {
                clearInterval(i);
                self.sound.alarm.stop();
                onComplete.apply(self);
            }
            self.scale.setTo(self.scale.x-scaleFactor, self.scale.y-scaleFactor);
            self.shadow.scale.setTo(self.scale.x-scaleFactor, self.scale.y-scaleFactor);
            self._flash();
        }, 250);
    this.sound.alarm.play();
    this.sound.fall.play();
    this._flash();
    return this;
};
BasicGame.Hero.prototype._flash = function() {
    var self = this;
        this.tint = 0xff0000;
    setTimeout(function() {
        self.tint = self.baseTint;
    }, 100);
    return this;
};
BasicGame.Hero.prototype._kill = function() {
    var explosion = new BasicGame.Explosion(this.game, this.position.x, this.position.y);
    this.game.add.existing(explosion);
    this.shadow.visible = false;
    this.visible = false;
    this.sound.thrust.stop();
    this.sound.die.play();
    // wait for explosion animation then kill
    explosion.onComplete(function() {
        Phaser.Sprite.prototype.kill.apply(this, arguments);
        this.shadow.kill();
    }, this);
    return this;
};
BasicGame.Hero.prototype.addFuel = function(value) {
    this.fuel = BasicGame.utils.clamp(this.fuel + value, 0, this.maxFuel);
    return this;
};
BasicGame.Hero.prototype.addHealth = function(value) {
    this.health = BasicGame.utils.clamp(this.health + value, 0, this.maxHealth);
    return this;
};
BasicGame.Hero.prototype.animate = function(value) {
    var inputLeft   = this.cursors.left.isDown || this.controller.left,
        inputRight  = this.cursors.right.isDown || this.controller.right,
        inputUp     = this.cursors.up.isDown || this.controller.up,
        inputDown   = this.cursors.down.isDown || this.controller.down;
    if (inputLeft || inputRight) {
        // do nothing so left an right animations play
    } else if ((inputUp || inputDown) && this.fuel > 0) {
        //  Stand still thrust
        this.frame = this.defaultFrame;
        this.animations.stop();
    } else {
        // stand still
        this.frame = this.stillDefaultFrame;
        this.animations.stop();
    }
    return this;
};
BasicGame.Hero.prototype.attack = function() {
    if (this.weapon && this.fuel > 0 && this.health > 0) {
        if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) || this.controller.fire) {
            this.weapon.attack(this.position.x, this.position.y-32);
        }
    }
    return this;
};
BasicGame.Hero.prototype.collide = function() {
    this._flash();
    // should we fade when hit?
    // this.alpha = this.health/this.maxHealth;
    // this.shadow.alpha = this.alpha;
    return this;
};
BasicGame.Hero.prototype.move = function() {
    if (this.cursors.left.isDown || this.controller.left) {
        //  Move to the left
        this.body.velocity.x += -this.speed;
    } else if (this.cursors.right.isDown || this.controller.right) {
        //  Move to the right
        this.body.velocity.x += this.speed;
    }

    if (this.cursors.up.isDown || this.controller.up) {
        this.body.velocity.y += -this.speed;
    } else if (this.cursors.down.isDown || this.controller.down) {
        this.body.velocity.y += this.speed;
    }
    // update shadow
    this.body.position.copyTo(this.shadow);
    return this;
};
BasicGame.Hero.prototype.onLeft = function() {
    this.animations.play('left');
};
BasicGame.Hero.prototype.onRight = function() {
    this.animations.play('right');
};
BasicGame.Hero.prototype.reset = function() {
    Phaser.Sprite.prototype.reset.apply(this, arguments);
    this._init();
    return this;
};
BasicGame.Hero.prototype.setWeapon = function(weapon) {
    this.weapon = weapon;
    return this;
};
BasicGame.Hero.prototype.update = function() {
    this.move();
    this.animate();
    this.attack();

    // fuel
    if (this.cursors.left.isDown || this.cursors.right.isDown ||
        this.cursors.up.isDown || this.cursors.down.isDown ||
        this.controller.left || this.controller.right ||
        this.controller.up || this.controller.down) {
        this.fuel -= this.fuelCost.move;
    } else {
        this.fuel -= this.fuelCost.idle;
    }
};
