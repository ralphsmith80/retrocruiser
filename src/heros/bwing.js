BasicGame.BWing = function(game, x, y) {
    var name = 'b-wing';
    BasicGame.Hero.call(this, game, x, y, name);

    this.scale.setTo(2, 2);

    this.weapon = null;

    this.animations.add('left', [1, 0], 3, false);
    this.animations.add('right', [3, 4], 3, false);
    this.defaultFrame = 2;
    this.stillDefaultFrame = this.defaultFrame;

    // shadow
    this.shadow = this.game.add.sprite(this.x, this.y, name, this.defaultFrame);
    this.shadow.tint = '#000';
    this.shadow.alpha = 0.3;
    this.shadow.scale.setTo(this.scale.x*0.75, this.scale.y*0.75);
};
BasicGame.BWing.prototype = Object.create(BasicGame.Hero.prototype);
BasicGame.BWing.constructor = BasicGame.BWing;

BasicGame.BWing.prototype.move = function() {
    BasicGame.Hero.prototype.move.apply(this, arguments);
    // shift the ship shadow
    this.shadow.position.setTo(this.body.position.x-(this.width/2),
        this.body.position.y-(this.height/2));
    return this;
};
