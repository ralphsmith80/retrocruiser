BasicGame.Hornet = function(game, x, y) {
    var name = 'hornet';
    BasicGame.Hero.call(this, game, x, y, name);

    this.weapon = null;

    this.animations.add('left', [3, 1], 3, false);
    this.animations.add('right', [7, 9], 3, false);
    this.defaultFrame = 5;

    this.animations.add('no-fuel-left', [2, 0], 3, false);
    this.animations.add('no-fuel-right', [6, 8], 3, false);
    this.stillDefaultFrame = 4;

    // shadow
    this.shadow = this.game.add.sprite(this.x, this.y, 'hornet-shadow', this.stillDefaultFrame);
};
BasicGame.Hornet.prototype = Object.create(BasicGame.Hero.prototype);
BasicGame.Hornet.constructor = BasicGame.Hornet;

BasicGame.Hornet.prototype.onLeft = function() {
    if (this.fuel <= 0) {
        this.animations.play('no-fuel-left');
    } else {
        this.animations.play('left');
    }
};
BasicGame.Hornet.prototype.onRight = function() {
    if (this.fuel <= 0) {
        this.animations.play('no-fuel-right');
    } else {
        this.animations.play('right');
    }
};
