// boost base class/interface
BasicGame.Boost = function(game, x, y, name, frame) {
    Phaser.Sprite.call(this, game, x, y, name, frame);
    this.anchor.setTo(0.5, 0.5);

    this.game.physics.arcade.enableBody(this);
    this.checkWorldBounds = true;
    this.outOfBoundsKill = true;

    this.sound = this.game.add.audio('boost', 1);
};
BasicGame.Boost.prototype = Object.create(Phaser.Sprite.prototype);
BasicGame.Boost.constructor = BasicGame.Boost;
BasicGame.Boost.prototype._init = function() {
    return this;
};
BasicGame.Boost.prototype.attack = function() {
    return this;
};
BasicGame.Boost.prototype.collect = function() {
    this.kill();
    this.sound.play();
    return this;
};
BasicGame.Boost.prototype.reset = function() {
    Phaser.Sprite.prototype.reset.apply(this, arguments);
    this._init();
    return this;
};
