BasicGame.HealthBoost = function(game, x, y, frame) {
    var name = 'health-boost';
    BasicGame.Boost.call(this, game, x, y, name, frame);
    this.name = name;
};
BasicGame.HealthBoost.prototype = Object.create(BasicGame.Boost.prototype);
BasicGame.HealthBoost.constructor = BasicGame.HealthBoost;
BasicGame.HealthBoost.prototype._init = function() {
    var velocityX = game.rnd.integerInRange(-50, 50),
        velocityY = game.rnd.integerInRange(20, 100);
    // 10% boost
    this.boost = 10;
    // 10% spawn chance
    this.spawnChance = 10;
    this.body.velocity.setTo(velocityX, velocityY);
    return this;
};
