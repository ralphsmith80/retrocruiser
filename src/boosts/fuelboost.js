BasicGame.FuelBoost = function(game, x, y, frame) {
    var name = 'fuel-boost';
    BasicGame.Boost.call(this, game, x, y, name, frame);
    this.name = name;
};
BasicGame.FuelBoost.prototype = Object.create(BasicGame.Boost.prototype);
BasicGame.FuelBoost.constructor = BasicGame.FuelBoost;
BasicGame.FuelBoost.prototype._init = function() {
    var velocityX = game.rnd.integerInRange(-50, 50),
        velocityY = game.rnd.integerInRange(20, 100);
    // 5% boost
    this.boost = 5;
    // 20% spawn chance
    this.spawnChance = 20;
    this.body.velocity.setTo(velocityX, velocityY);
    return this;
};
