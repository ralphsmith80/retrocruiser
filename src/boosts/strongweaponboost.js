BasicGame.StrongWeaponBoost = function(game, x, y, frame) {
    var name = 'strong-weapon';
    BasicGame.Boost.call(this, game, x, y, name, frame);
    this.name = name + '-boost';
};
BasicGame.StrongWeaponBoost.prototype = Object.create(BasicGame.Boost.prototype);
BasicGame.StrongWeaponBoost.constructor = BasicGame.StrongWeaponBoost;
BasicGame.StrongWeaponBoost.prototype._init = function() {
    var velocityX = game.rnd.integerInRange(-50, 50),
        velocityY = game.rnd.integerInRange(20, 100);
    // strong weapon boost
    this.boost = 'strong';
    // 3% spawn chance
    this.spawnChance = 3;
    this.scale.setTo(0.4, 0.4);
    this.body.velocity.setTo(velocityX, velocityY);
    return this;
};
