BasicGame.MediumWeaponBoost = function(game, x, y, frame) {
    var name = 'medium-weapon';
    BasicGame.Boost.call(this, game, x, y, name, frame);
    this.name = name + '-boost';
};
BasicGame.MediumWeaponBoost.prototype = Object.create(BasicGame.Boost.prototype);
BasicGame.MediumWeaponBoost.constructor = BasicGame.MediumWeaponBoost;
BasicGame.MediumWeaponBoost.prototype._init = function() {
    var velocityX = game.rnd.integerInRange(-50, 50),
        velocityY = game.rnd.integerInRange(20, 100);
    // medium weapon boost
    this.boost = 'medium';
    // 10% spawn chance
    this.spawnChance = 7;
    this.scale.setTo(0.5, 0.5);
    this.body.velocity.setTo(velocityX, velocityY);
    return this;
};
