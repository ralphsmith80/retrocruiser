BasicGame.BoostGenerator = function(game, Sprite) {
    BasicGame.RecycleGroup.call(this, game, Sprite);

    // init a sprite for spawn behavior
    var sprite = new Sprite(game)._init();
    this.spawnChance = sprite.spawnChance;
};
BasicGame.BoostGenerator.prototype = Object.create(BasicGame.RecycleGroup.prototype);
BasicGame.BoostGenerator.constructor = BasicGame.BoostGenerator;
// define the spawn behavior
BasicGame.BoostGenerator.prototype.spawn = function(x, y) {
    var sprite = null;
    // 100 will alway be greater than 0..99
    // 0 will never be greater than 0..99
    if (this.game.rnd.integerInRange(0, 99) < this.spawnChance) {
        sprite = BasicGame.RecycleGroup.prototype.spawn.call(this, x, y);
    }
    return sprite;
};
